const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  name: String,
  gender: String
})

module.exports = Users = mongoose.model('Users', userSchema)